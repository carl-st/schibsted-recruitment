# Simple JS App
Application is using webpack for bundilng, es6+ with scss transpiling and optimization.

Prism.js is a library for syntax higlighting.

## Running
1. Install needed dependiencies with `npm i`
2. Run `npm run start`
3. Open `localhost:9000` to access the app

## Production build
1. Install needed dependiencies with `npm i`
2. Run `npm run build`

## Testing
1. Run `npm run test` to perform Jest tests
2. Run `npm run lint` to perform linitng 

Testing is configured to be run with Jest and Sinon. There is one example test of api.js but it crashes as node.js does not have XMLHttpRequest defined.
Test in current form would need library like https://github.com/driverdan/node-XMLHttpRequest, because tests need to be performed on node environment.
