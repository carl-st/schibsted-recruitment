import './../styles/main.scss';
import './../styles/content.scss';
import './../styles/navigation.scss';
import Prism from 'prismjs';
import 'prismjs/themes/prism.css';
import 'prismjs/components/prism-http.min.js';
import 'prismjs/components/prism-json.min.js';
import 'prismjs/components/prism-markup.min.js';
import 'prismjs/components/prism-lua.min.js';
import { paths, globalConstants } from './../constants/';
import { fetchData } from './api';

Prism.highlightAll();

const jsonString = globalConstants.JSON_STRING;
const xmlString = globalConstants.XML_STRING;
let fileType = jsonString;

const xmlButton = document.getElementById("xml-button");
const jsonButton = document.getElementById("json-button");
const callApiButton = document.getElementById("call-api-button");
const urlContainer = document.getElementById("url-container");
const codeContainer = document.getElementById("code-container");
const headersContainer = document.getElementById("headers-container");

callApiButton.addEventListener(globalConstants.CLICK_STRING, handleOnCallApiButtonClick);
jsonButton.addEventListener(globalConstants.CLICK_STRING, setJSONType);
xmlButton.addEventListener(globalConstants.CLICK_STRING, setXMLType);

urlContainer.innerText = "GET /advertisers/?format=api";
codeContainer.innerText = "Make a request to show its response details here.";

function handleOnCallApiButtonClick () {
    switch (fileType) {
        case jsonString:
            fetchData(globalConstants.GET_STRING, paths.JSON_FILE, handleResponse);
            break;
        case xmlString:
            fetchData(globalConstants.GET_STRING, paths.XML_FILE, handleResponse);
            break;
        default:
            break;
    }
}

function handleResponse (method, status, readyState, responseHeaders, responseText) {
    if (readyState == 4 && status == 200) {
        const requestTypeAndStatus = `${method} ${status}\n`;
        headersContainer.innerText = `${requestTypeAndStatus}${responseHeaders}\n`;
        codeContainer.innerText =  responseText;
        Prism.highlightAll();
    } else if (readyState == 4 && status == 404) {
        codeContainer.innerText = globalConstants.NOT_FOUND;
    }
}

function setJSONType () {
    callApiButton.innerHTML = jsonString;
    codeContainer.className = globalConstants.JSON_LANG;
    fileType = jsonString;
}

function setXMLType () {
    callApiButton.innerHTML = xmlString;
    codeContainer.className = globalConstants.MARKUP_LANG;
    fileType = xmlString;
}