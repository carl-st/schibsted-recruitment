function fetchData(method, url, callback) {
    let xhttp = new XMLHttpRequest();
    xhttp.open(method, url, true);
    xhttp.onreadystatechange = function() {
        const responseHeaders = this.getAllResponseHeaders();
        callback(method, this.status, this.readyState, responseHeaders, this.responseText);
    };
    xhttp.send();
}

export { fetchData };
