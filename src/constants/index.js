import paths from './paths';
import globalConstants from './globalConstants';

export {
    paths,
    globalConstants
};
