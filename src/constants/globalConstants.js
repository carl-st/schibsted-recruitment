export default {
    JSON_STRING: "JSON",
    XML_STRING: "XML",
    GET_STRING: "GET",
    CLICK_STRING: "click",
    NOT_FOUND: "File was not found.",
    JSON_LANG: "language-json",
    MARKUP_LANG: "language-markup"
};