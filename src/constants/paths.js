export default {
    JSON_FILE: "assets/response.json",
    TEST_FILE: "assets/test.json",
    XML_FILE: "assets/response.xml"
};